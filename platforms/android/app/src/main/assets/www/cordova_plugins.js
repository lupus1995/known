cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
	  "id": "cordova-plugin-backbutton.Backbutton",
	  "file": "plugins/cordova-plugin-backbutton/www/Backbutton.js",
	  "pluginId": "cordova-plugin-backbutton",
	  "clobbers": [
		  "navigator.Backbutton"
	  ]
  },
	{
		"id": "cordova-plugin-inapppurchase.InAppBillingV3",
		"file": "plugins/cordova-plugin-inapppurchase/www/index-android.js",
		"pluginId": "cordova-plugin-inapppurchase",
		"merges": [
			"inAppPurchase"
		]
	},
	{
		"id": "cordova-plugin-splashscreen.SplashScreen",
		"file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
		"pluginId": "cordova-plugin-splashscreen",
		"clobbers": [
			"navigator.splashscreen"
		]
	},
	{
		"id": "es6-promise-plugin.Promise",
		"file": "plugins/es6-promise-plugin/www/promise.js",
		"pluginId": "es6-promise-plugin",
		"runs": true
	},
	{
		"id": "cordova-plugin-x-socialsharing.SocialSharing",
		"file": "plugins/cordova-plugin-x-socialsharing/www/SocialSharing.js",
		"pluginId": "cordova-plugin-x-socialsharing",
		"clobbers": [
			"window.plugins.socialsharing"
		]
	},
	{
		"id": "onesignal-cordova-plugin.OneSignal",
		"file": "plugins/onesignal-cordova-plugin/www/OneSignal.js",
		"pluginId": "onesignal-cordova-plugin",
		"clobbers": [
			"OneSignal"
		]
	},
	{
		"id": "cordova-connectivity-monitor.connectivity",
		"file": "plugins/cordova-connectivity-monitor/www/connectivity.js",
		"pluginId": "cordova-connectivity-monitor",
		"clobbers": [
			"window.connectivity"
		]
	},
	{
		"id": "cordova-admob.AdMobAds",
		"file": "plugins/cordova-admob/www/admob.js",
		"pluginId": "cordova-admob",
		"clobbers": [
			"window.admob",
			"window.tappx"
		]
	}
];
module.exports.metadata = 
// TOP OF METADATA
{
	"cordova-plugin-backbutton": "0.3.0",
	"cordova-plugin-inapppurchase": "1.1.0",
  "cordova-plugin-splashscreen": "5.0.2",
	"cordova-plugin-whitelist": "1.3.3",
	"es6-promise-plugin": "4.2.2",
	"cordova-plugin-x-socialsharing": "5.4.4",
	"onesignal-cordova-plugin": "2.4.6",
	"cordova-connectivity-monitor": "1.2.2",
	"cordova-play-services-version-adapter": "1.0.2",
	"cordova-admob": "5.1.0"
};
// BOTTOM OF METADATA
});
