import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-command',
    templateUrl: './command.component.html',
    styleUrls: ['./command.component.css']
})
export class CommandComponent implements OnInit {
    
    constructor(private router: Router) {
    }

    ngOnInit() {
    }

    onBack() {
        this.router.navigate(['/menu']);
    }

}
