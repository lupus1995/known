import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Product} from "../../shared/interfaces/product.inteface";

declare var $: any;
declare var inAppPurchase: any;

@Component({
    selector: 'app-support',
    templateUrl: './support.component.html',
    styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit {
    buyProductsAfterRestore;
    products: Product[];
    
    constructor(private router: Router) {
    }
    
    ngOnInit() {
        let component = this;
        inAppPurchase
            .restorePurchases()
            .then((restore: Product[]) => {
                let product = ['99_ruble', '299_ruble', '499_ruble', '999_ruble'];
                restore.forEach((value, index) => {
                    // component.printObject(value);
                    if (product.indexOf(value.productId) != -1) {
                        product.splice(product.indexOf(value.productId), 1);
                    }
                });
                component.buyProductsAfterRestore = product;
            })
            .catch(function (err) {
                // alert('ошибка проверки покупки')
            })
            .then(() => {
                inAppPurchase
                    .getProducts(component.buyProductsAfterRestore)
                    .then(function (products: Product[]) {
                        component.products = products;
                        products.forEach((value) => {
                            // component.printObject(value);
                        })
                        
                    })
                    .catch(function (err) {
                        // component.printObject(err);
                    });
            })
        ;
    }
    
    onBack() {
        this.router.navigate(['/menu']);
    }
    
    viewPayments() {
        $('.layout').fadeIn(() => {
            $('.modal-message').fadeIn(() => {
                $('.modal-message').css({
                    'display': 'flex',
                }).animate({
                    'opacity': 1
                }, 400)
            })
        })
    }
    
    closeLayout() {
        $('.modal-message').fadeOut(() => {
            $('.layout').fadeOut();
        });
    }
    
    payment(buy, index) {
        let component = this;
        inAppPurchase
            .buy(buy)
            .then(function (data) {
                component.products.splice(index, 1);
                component.closeLayout();
            })
            .catch(function (err) {
                console.log(err);
            });
    }
    
    private printObject(o) {
        var out = '';
        for (var p in o) {
            out += p + ': ' + o[p] + '\n';
        }
        // alert(out);
    }
}
