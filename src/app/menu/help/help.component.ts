import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {environment} from "../../../environments/environment";

declare var $: any;



@Component({
    selector: 'app-help',
    templateUrl: './help.component.html',
    styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {
    viewLinkOnMain: boolean = false;
    constructor(private router: Router) {
    }

    ngOnInit() {
        let component = this;
        let firstVisit = localStorage.getItem(environment.firstVisit);
        console.log(firstVisit);
        if (!firstVisit) {
            localStorage.setItem(environment.firstVisit, 'true');
            component.viewLinkOnMain = true;
        }
        $(document).ready(() => {
            $('.help-slick').slick({
                arrows: false, dots: true, infinite: false,
                customPaging: function (slider, i) {
                    return '<img src="assets/image/dots/dot_help_o.png" height="16" width="16" />' +
                        '<img src="assets/image/dots/dot_help.png" height="16" width="16" />';
                },
            });
        });
    }

    onBack() {
        if (this.viewLinkOnMain) {
            this.router.navigate(['/main']);
            return;
        }
        this.router.navigate(['/menu']);
    }
    
    
}
