import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

    constructor(private router: Router) {
    }

    ngOnInit() {
    }

    onOpenMessage() {
        window.open('mailto:znaushiy.app@mail.ru', '_system');
        return false;
    }

    onBack() {
        this.router.navigate(['/main']);
    }

}
