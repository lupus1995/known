import {Component, Input, OnInit} from '@angular/core';

declare var $: any;

@Component({
	selector: 'app-card-description-image',
	templateUrl: './card-description-image.component.html',
	styleUrls: ['./card-description-image.component.css']
})
export class CardDescriptionImageComponent implements OnInit {
	@Input() content: { path: string, contentItemType: string, mainText: string };
	@Input() id: number;
	
	constructor() {
	}
	
	ngOnInit() {
		let component = this;
		$(document).ready(function () {
			$('#desc-container-img-' + component.id).slickLightbox({
				itemSelector: 'a',
				navigateByKeyboard: false,
				caption: () => {
					return $('<p/>', {
						text: component.content.mainText
					}).css({
						'marginTop': '10px'
					})[0].outerHTML;
				}
			});
		});
	}
	
}
