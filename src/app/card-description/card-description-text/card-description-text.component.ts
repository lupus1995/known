import {Component, Input, OnInit} from '@angular/core';
import {$t} from "codelyzer/angular/styles/chars";

declare var $: any;

@Component({
	selector: 'app-card-description-text',
	templateUrl: './card-description-text.component.html',
	styleUrls: ['./card-description-text.component.css']
})
export class CardDescriptionTextComponent implements OnInit {
	
	@Input() content: string;
	tagP;
	
	constructor() {
	}
	
	ngOnInit() {
		this.content = this.content
			.replace(/</g, '(')
			.replace(/>/g, ')')
			.replace(/\(/g, '<span class="card-description text-yellow">')
			.replace(/\)/g, '</span>');
		this.tagP = $('<p/>', {
			html: this.content,
		});
		
	}
	
}
