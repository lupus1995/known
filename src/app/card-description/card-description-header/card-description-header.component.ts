import {Component, Input, OnInit} from '@angular/core';

@Component({
	selector: 'app-card-description-header',
	templateUrl: './card-description-header.component.html',
	styleUrls: ['./card-description-header.component.css']
})
export class CardDescriptionHeaderComponent implements OnInit {
	
	@Input() content: string;
	
	constructor() {
	}
	
	ngOnInit() {
	}
	
}
