import {Component, Input, OnInit} from '@angular/core';

@Component({
	selector: 'app-card-description-text-in-box',
	templateUrl: './card-description-text-in-box.component.html',
	styleUrls: ['./card-description-text-in-box.component.css']
})
export class CardDescriptionTextInBoxComponent implements OnInit {
	
	@Input() content:string;
	constructor() {
	}
	
	ngOnInit() {
	}
	
}
