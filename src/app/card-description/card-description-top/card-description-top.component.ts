import {Component, Input, NgZone, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from "../../../environments/environment";
import {Subthemes} from "../../shared/models/subthemes.model";

@Component({
	selector: 'app-card-description-top',
	templateUrl: './card-description-top.component.html',
	styleUrls: ['./card-description-top.component.css']
})
export class CardDescriptionTopComponent implements OnInit {
	@Input() name: string;
	id: string;
	themeId: string;
	
	constructor(
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private ngZone: NgZone
	) {
	}
	
	ngOnInit() {
		this.id = this.activatedRoute.snapshot.params.id;
		this.themeId = this.activatedRoute.snapshot.params.subjectId;
	}
	
	onBack() {
		console.log(this.activatedRoute.snapshot.params);
		if (this.activatedRoute.snapshot.params.favorite == '1') {
			this.router.navigate(['/favorite']);
			return;
		}
		let subThemes: Subthemes[] = JSON.parse(localStorage.getItem(environment.subThemes));
		subThemes.forEach((value) => {
			if (value.id == +this.themeId) {
				this.router.navigate(['/cards', this.themeId, 'theme', value.subjectId]);
				return
			}
		});
		
		if (this.activatedRoute.snapshot.params.subjectId && this.activatedRoute.snapshot.params.subjectId.indexOf('favorite') != -1) {
			this.router.navigate(['/favorite']);
			return;
		}
		
		if (this.router.url.indexOf('dictionary') != -1) {
			this.ngZone.run(() => this.router.navigate([localStorage.getItem(environment.prevLinkDictionary)])).then();
		}
		
	}
	
	
}
