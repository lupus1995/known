import {Component, NgZone, OnInit} from "@angular/core";
import {MainService} from "../shared/servises/main.service";
import {Cards} from "../shared/models/cards.model";
import {ActivatedRoute, Router} from "@angular/router";
import {DetailItems} from "../shared/models/detail-items.model";
import {environment} from "../../environments/environment";

declare var $: any;

@Component({
	selector: 'app-card-description',
	templateUrl: './card-description.component.html',
	styleUrls: ['./card-description.component.css']
})
export class CardDescriptionComponent implements OnInit {
	
	id: string;
	subjectId: string | number;
	cards: Cards[];
	detail: DetailItems;
	name: string;
	viewDescription: boolean;
	
	constructor(
		private mainService: MainService,
		private route: ActivatedRoute,
		private router: Router,
		private ngZone: NgZone
		) {
	}
	
	ngOnInit() {
		this.id = this.route.snapshot.params.id;
		this.subjectId = this.route.snapshot.params.subjectId;
		this.viewDescription = (this.subjectId == '2');
		
		localStorage.setItem(environment.currentSlide, this.id);
		this.cards = JSON.parse(localStorage.getItem(environment.cards));
		this.setDetail(this.cards);
		
		if (!this.detail) {
			this.mainService
				.getCards(this.subjectId)
				.then((cards: Cards[]) => {
					this.setDetail(cards)
				})
		}
		
		
		$(document).on('click', '.text-yellow', (e: Event) => {
			if (this.router.url.indexOf('subjectId') != -1) {
				let name = $(e.target).parent().siblings('.card-item-title').text();
				let slideId = (+$(e.target).closest('.card-item').attr('data-card'));
				localStorage.setItem(environment.currentSlide, slideId.toString());
				localStorage.setItem(environment.prevLinkDictionary, this.router.url);
				this.ngZone.run(() => this.router.navigate(['/dictionary'],
					{
						queryParams: {search: $(e.target).text(), name: name}
					})).then();
			}
		});
	}
	
	setViewDescription(event: boolean) {
		this.viewDescription = event;
	}
	
	private setDetail(cards: Cards[]) {
		cards.forEach((value: any, index) => {
			if (this.id == value.id) {
				this.detail = value.detail.detailItems;
				this.name = value.name;
			}
		})
	}
	
}
