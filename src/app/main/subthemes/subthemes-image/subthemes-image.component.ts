import {Component, Input, OnInit} from '@angular/core';

declare var $: any;

@Component({
	selector: 'app-subthemes-image',
	templateUrl: './subthemes-image.component.html',
	styleUrls: ['./subthemes-image.component.css']
})
export class SubthemesImageComponent implements OnInit {
	@Input() src: string;
	@Input() id: number;
	
	constructor() {
	}
	
	ngOnInit() {
		$(document).ready(()=>{
			$('#subthemes-' + this.id).lazyload({
				effect : "fadeIn"
			});
		})
		
	}
	
}
