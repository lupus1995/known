import {Component, OnDestroy, OnInit} from "@angular/core";
import {MainService} from "../../shared/servises/main.service";
import {Subthemes} from "../../shared/models/subthemes.model";
import {ProgressBar} from "../../shared/models/progress-bar";
import {environment} from "../../../environments/environment";
import {Router} from "@angular/router";
import {Cards} from "../../shared/models/cards.model";

declare var $: any;

@Component({
	selector: 'app-subthemes',
	templateUrl: './subthemes.component.html',
	styleUrls: ['./subthemes.component.css']
})
export class SubthemesComponent implements OnInit, OnDestroy {
	subthemes: Subthemes[];
	loadPage: boolean = false;
	token: string;
	progressBars: ProgressBar[];
	
	constructor(private mainService: MainService, private router: Router) {
	}
	
	ngOnInit() {
		let idCurrentThemes = localStorage.getItem(environment.currentThemes);
		
		this.progressBars = JSON.parse(localStorage.getItem(environment.progressBar));
		
		if (idCurrentThemes && idCurrentThemes !== 'null' && idCurrentThemes !== 'undefined') {
			console.log(idCurrentThemes);
			this.mainService.getSubThemes(+idCurrentThemes)
				.then((data: Subthemes[]) => {
					this.subthemes = data;
					localStorage.setItem(environment.currentThemes, '');
					this.initJquery();
					this.loadPage = true;
				})
		} else {
			this.mainService.getSubThemes()
				.then((data: Subthemes[]) => {
					this.subthemes = data;
					this.initJquery();
					this.loadPage = true;
				});
		}
	}
	
	getCardTheme(subthemeId: string | number, url: any) {
		$('.layout').fadeIn(() => {
			$('.preloader').fadeIn(() => {
				this.mainService.getCards(subthemeId)
					.then((data: Cards[]) => {
						if (data) {
							this.router.navigate(url);
						} else {
							$('.preloader').fadeOut(() => {
								$('.modal-message').fadeIn(() => {
									$('.modal-message').css({
										'display': 'flex',
									}).animate({
										'opacity': 1
									}, 400, () => {
										setTimeout(() => {
											$('.modal-message').fadeOut(() => {
												$('.layout').fadeOut();
											})
										}, 1000)
									})
								})
							})
						}
					})
			})
		});
		
	}
	
	ngOnDestroy() {
	}
	
	private initJquery() {
		let component = this;
		$(document).ready(() => {
			$('.slick').on('swipe', function (event, slick, direction) {
				let id = $(event.target).find('.slick-active .slick-item').attr('data-theme');
				component.mainService.getSubThemes(id)
					.then((data: Subthemes[]) => {
						component.subthemes = data;
					})
			});
		});
	}
}
