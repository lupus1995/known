import {Component, OnInit} from "@angular/core";
import {MainService} from "../../shared/servises/main.service";
import {Theme} from "../../shared/models/theme.model";
import {ProgressBar} from "../../shared/models/progress-bar";
import {environment} from "../../../environments/environment";

// import * as $ from 'jquery';
declare var $: any;

@Component({
	selector: 'app-themes',
	templateUrl: './themes.component.html',
	styleUrls: ['./themes.component.css']
})
export class ThemesComponent implements OnInit {
	themes: Theme[];
	progressBars: ProgressBar[];
	
	constructor(private mainService: MainService) {
	}
	
	ngOnInit() {
		this.progressBars = JSON.parse(localStorage.getItem(environment.progressBar));
		this.mainService.getTheme().then((themes: Theme[]) => {
			this.themes = themes;
			this.setLearning();
			$(document).ready(() => {
				$('.slick').slick({
					arrows: false,
					adaptiveHeight: true,
					variableWidth: true
				});
				this.lsMoveToCard()
			});
		})
	}
	
	//переход на карту, после возвращения с подробной инфы о карте
	lsMoveToCard() {
		let id = localStorage.getItem(environment.currentThemes);
		console.log(id);
		if (id && id !== 'null' && id !== 'undefined' && id !== '') {
			this.moveToCardId(+id)
		} else {
			console.log(123);
			this.moveToCardId(1)
		}
		
	}
	
	moveToCardId(cardId: number) {
		let slide = $('.slick').find('.slick-item[data-card=' + cardId + ']').parents('.slick-slide'),
			id = slide.attr('data-slick-index');
		$('.slick').slick('slickGoTo', +id);
	}
	
	private setLearning() {
		if (this.progressBars) {
			this.themes.forEach((value, index, array) => {
				array[index].learning = 0;
				this.progressBars.forEach((progress) => {
					if (value.id == progress.parentId && progress.countReadCards == progress.countCards) {
						array[index].learning = array[index].learning + 1;
					}
				});
			});
		}
	}
	
}
