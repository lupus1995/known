import {Component, OnInit} from "@angular/core";
import {MainService} from "../shared/servises/main.service";
import {Subscription} from "rxjs";
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";

@Component({
	selector: 'app-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
	subscribe: Subscription;
	dataSubscribe: Subscription;
	token: string;
	loadPage:boolean = false;
	
	constructor(
		private mainServices: MainService,
		private route: Router
	) {
	}
	
	ngOnInit() {
		let firstVisit = localStorage.getItem(environment.firstVisit)
		if (!firstVisit) {
			this.route.navigate(['/help']);
		}
		this.mainServices.auth().then(()=>{
			this.loadPage = true;
		})
	}
	
}
