export interface Product {
    productId: string,
    title: string,
    description: string,
    currency: string,
    price: string,
    priceAsDecimal: string
}
