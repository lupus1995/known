import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {isNullOrUndefined} from "util";
import {environment} from "../../../environments/environment";
import {Cards} from "../models/cards.model";
import {Auth} from "../models/auth.model";
import {Subthemes} from "../models/subthemes.model";
import {Theme} from "../models/theme.model";
import {Votes} from '../models/votes';
import {Polls} from "../models/polls.model";
import {Product} from "../interfaces/product.inteface";

interface Data {
	data: Auth | Cards | Subthemes | Theme
}

declare var inAppPurchase;

declare var $: any;

@Injectable({
	providedIn: 'root'
})
export class MainService {
	
	formData = new FormData;
	url = environment.url;
	
	constructor(private http: HttpClient) {
	}
	
	auth(): Promise<Auth | void> {
		let data = {
			client_id: '3',
			client_secret: 'CcwF3SDjD9dgqYnwUPHT7DXNctpRVh3CejAEH7X7',
			scope: '',
			grant_type: 'client_credentials'
		};
		
		Object.keys(data).forEach(key => {
			if (!isNullOrUndefined(data[key])) this.formData.append(key, data[key].toString())
		});
		return this.http.post(this.url + '/oauth/token', this.formData)
			.toPromise()
			.then((data: Auth) => {
				localStorage.setItem(environment.token, data.access_token)
			}, () => {
			
			})
	}
	
	getTheme(): Promise<Theme[] | void> {
		let token = this.getToken();
		let headers = this.setHeaders(token);
		return this.http.get(this.url + '/api/v1/themes', {headers: headers})
			.toPromise()
			.then((data: any) => {
					//возможно надо будет засунуть ls
					this.localStorage(environment.themes, data.data)
					return data.data;
				},
				() => {
				
				})
	}
	
	getSubThemes(id: string | number = 1): Promise<Subthemes[] | void> {
		let token = this.getToken();
		let headers = this.setHeaders(token);
		return this.http.get(this.url + `/api/v1/subthemes/${id}`, {headers: headers})
			.toPromise()
			.then((data: any) => {
				this.localStorage(environment.subThemes, data.data);
				return data.data;
			}, () => {
			
			})
	}
	
	getPolls(id: string | number = 1): Promise<Polls[] | void> {
		let token = this.getToken();
		let headers = this.setHeaders(token);
		return this.http.get(this.url + `/api/v1/votes`, {headers: headers})
			.toPromise()
			.then((data: any) => {
				this.localStorage(environment.poll, data.data);
				return data.data;
			}, () => {
			
			})
	}
	
	setPolls(id: number = 0): Promise<Polls[] | void> {
		let token = this.getToken();
		let headers = this.setHeaders(token);
		return this.http.put(this.url + `/api/v1/voice/${id}`, '', {headers: headers})
			.toPromise()
			.then((data: any) => {
				return data.data;
			}, () => {
			
			})
	}
	
	getCards(subthemeId): Promise<Cards[] | void> {
		let token = this.getToken();
		let headers = this.setHeaders(token);
		return this.http.get(this.url + `/api/v1/cards/${subthemeId}`, {headers: headers})
			.toPromise()
			.then((data: any) => {
				if (data) {
					this.localStorage(environment.cards, data.data);
					return data.data;
				}
				return null;
			})
	}
	
	getTests(cardId: number): Promise<Votes[] | void> {
		let token = this.getToken();
		let headers = this.setHeaders(token);
		return this.http.get(this.url + `/api/v1/tests/${cardId}`, {headers: headers})
			.toPromise()
			.then((data: any) => {
				this.localStorage(environment.votes, data.data);
				return data.data;
			})
	}
	
	getDictionary(search: string) {
		let token = this.getToken();
		let headers = this.setHeaders(token);
		return this.http.get(this.url +`/api/v1/references?search=${search}`, {headers: headers})
			.toPromise()
			.then((data:any)=>{
				return data.data[0].detailItems[0].content;
			}).catch(() => {
				alert('Ошибка загрузки контента!')
				$('.container-top button').trigger('click');
			})
	}
	
	getBuyProduct(): Promise<Product[]> {
		// alert('попали в запрос')
		return inAppPurchase
			.restorePurchases()
			.then(function (data: Product[]) {
				return data
			})
			.catch(function (err) {
				// alert('ошибка проверки покупки')
				// alert(err)
			});
	}
	
	buyProduct(product: string): Promise<any> {
		return inAppPurchase
			.buy(product)
			.then(function (data) {
				return data
			})
			.catch(function (err) {
				console.log(err);
			});
	}
	
	private setHeaders(token: string) {
		let headers = new HttpHeaders({
			// 'Access-Control-Allow-Origin': '*',
			'Authorization': 'Bearer ' + token
		});
		
		return headers;
	}
	
	private getToken() {
		return localStorage.getItem(environment.token)
	}
	
	private localStorage(name: string, data: Object[]) {
		localStorage.setItem(name, JSON.stringify(data));
	}
}
