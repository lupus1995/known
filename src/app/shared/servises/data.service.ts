import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/index";

@Injectable({
	providedIn: 'root'
})
export class DataService {
	private tokenSource = new BehaviorSubject('');
	currentToken = this.tokenSource.asObservable();
	
	private numberResultSource = new BehaviorSubject(0);
	currentNumberResultSource = this.numberResultSource.asObservable();
	
	constructor() {
	}
	
	changeToken(token: string) {
		this.tokenSource.next(token);
	}
	
	changeNumberResultSource(number: number) {
		this.numberResultSource.next(name);
	}
}
