export class DetailItems {
	constructor(public content:string,
	            public id:number,
	            public presentationType:string) {
	}
}