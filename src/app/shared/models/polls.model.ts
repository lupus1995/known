export class Polls {
    constructor(public bdImageName: string,
                public id: number,
                public name: string,
                public open: boolean,
                public subjectId: number,
                public subsubjectDescription: string) {
    }
}
