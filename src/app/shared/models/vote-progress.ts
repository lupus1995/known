export class VoteProgress {
    constructor(
        public voteId: number,
        public slidetId: number,
        public statusType: string
    ) {
    }
}
