export class Theme {
	constructor(public id: number,
	            public image: string,
	            public isActive: number,
	            public name: string,
	            public subjectDescription: string,
	            public openSubthemes: number,
	            public learning?: number
	) {
		
	}
}