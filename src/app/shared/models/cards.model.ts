import {Detail} from "./detail.model";
export class Cards {
	constructor(public id:number,
	            public subSubjectId:number,
	            public name:string,
	            public testButtonName:string,
	            public description:string,
	            public mainText:string,
	            public cardType:string, 
	            public separatorType:string,
	            public imagePath:string|null,
	            public fullImagePath:string,
	            public imageDescription:string,
	            public detail:Detail[]|Detail|null,
	            public testCount:number|null,
	            public text:string) {
	}
}
