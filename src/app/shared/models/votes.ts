export class Votes {
    constructor(
        public answerDescription: string,
        public id: number,
        public name: string,
        public answers: any,
        public typeAnswer?: string
    ) {

    }
}
