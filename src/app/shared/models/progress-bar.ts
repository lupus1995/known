export class ProgressBar {
	constructor(
		public subThemeId: number,
		public parentId:number,
		public countCards: number,
		public countReadCards: number
	) {
	}
}