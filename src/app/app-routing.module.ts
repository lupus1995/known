import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CardsComponent} from './cards/cards.component';
import {CardDescriptionComponent} from './card-description/card-description.component';
import {MenuComponent} from './menu/menu.component';
import {VotesComponent} from './votes/votes.component';
import {HelpComponent} from './menu/help/help.component';
import {CommandComponent} from './menu/command/command.component';
import {FavoriteComponent} from "./favorite/favorite.component";
import {PollComponent} from "./poll/poll.component";
import {DictionaryComponent} from "./dictionary/dictionary.component";
import {SupportComponent} from "./menu/support/support.component";
import {PrivateComponent} from "./private/private.component";

const routes: Routes = [
	{path: '', redirectTo: 'main', pathMatch: 'full'},
	{path: 'cards/:id/theme/:themeId', component: CardsComponent},
	{path: 'cards-description/:id/subjectId/:subjectId', component: CardDescriptionComponent},
	{path: 'menu', component: MenuComponent},
	{path: 'help', component: HelpComponent},
	{path: 'command', component: CommandComponent},
	{path: 'votes/:cardId', component: VotesComponent},
	{path: 'favorite', component: FavoriteComponent},
	{path: 'dictionary', component: DictionaryComponent},
    {path: 'poll', component: PollComponent},
    {path: 'support', component: SupportComponent},
    {path: 'private', component: PrivateComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
