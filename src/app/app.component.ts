import {Component, OnInit} from "@angular/core";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
	title = 'known';
	
	constructor() {
		
	}
	
	ngOnInit(){
		document.addEventListener("deviceready", onDeviceReady, false);
		
		function onDeviceReady() {
            // alert('сработал');
			var notificationOpenedCallback = function (jsonData) {
                console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
			};
			// @ts-ignore
			window.plugins.OneSignal
				.startInit("e35735e6-de58-4c62-8070-28872568de98")
				.handleNotificationOpened(notificationOpenedCallback)
				.endInit();
            
            // Set AdMobAds options:
            // @ts-ignore
            admob.setOptions({
                publisherId: "NONE",  // Required
                interstitialAdId: "ca-app-pub-7660507641366584/7733344541",  // Optional
                autoShowBanner: false,
                isTesting: false,
                overlap: true,
                // @ts-ignore
                adSize: admob.AD_SIZE.SMART_BANNER,
            });
            
            // @ts-ignore
            document.addEventListener(admob.events.onAdLoaded, onAdLoaded);
            // @ts-ignore
            document.addEventListener(admob.events.onAdFailedToLoad, function (e) {
            });
            // @ts-ignore
            document.addEventListener(admob.events.onAdOpened, function (e) {
            });
            // @ts-ignore
            document.addEventListener(admob.events.onAdClosed, function (e) {
            });
            // @ts-ignore
            document.addEventListener(admob.events.onAdLeftApplication, function (e) {
            });
            
            // @ts-ignore
            document.addEventListener("pause", onPause, false);
            // @ts-ignore
            document.addEventListener("resume", onResume, false);
		}
	}
 
 
}
