import {Component, OnDestroy, OnInit} from '@angular/core';
import {MainService} from '../shared/servises/main.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Votes} from '../shared/models/votes';
import {environment} from '../../environments/environment';
import {VoteProgress} from '../shared/models/vote-progress';
import {Cards} from "../shared/models/cards.model";
import {Subthemes} from "../shared/models/subthemes.model";
import {combineLatest, Subscription} from "rxjs";
import {Product} from "../shared/interfaces/product.inteface";

declare var $: any;

const answerText = {
    'good': 'Привильно!',
    'bad': 'Еще повезет',
    'dontknow': 'Бывает :(',
};

@Component({
    selector: 'app-votes',
    templateUrl: './votes.component.html',
    styleUrls: ['./votes.component.css']
})
export class VotesComponent implements OnInit, OnDestroy {
    id: string | number;
    tests: Votes[];
    btnText = 'Я не знаю :(';
    viewDescription: boolean;
    
    isBuyProduct: boolean = false;
    
    subscribe: Subscription;
    
    constructor(
        private mainService: MainService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        let stat = 'dontknow';
        let goodAnswers = 0;
        let voteProgress;
        let progress: VoteProgress;
        let progressType;
        let component = this;
        const _id = this.route.snapshot.params.cardId;
        console.log(_id);
    
        this.id = this.route.snapshot.params.cardId;
        localStorage.setItem(environment.currentSlide, _id);
        this.subscribe = combineLatest(
            this.mainService.getBuyProduct(),
            this.mainService.getTests(+this.id),
            (products: Product[], data: Votes[]) => {
                products.forEach((value) => {
                    if (value.productId == 'access3' || _id == '205') {
                        // alert('продукт был ранее куплен')
                        this.isBuyProduct = true;
                    }
                    // this.printObject(value);
                });
            
                this.tests = data;
                this.tests.forEach((value, index, array) => {
                    let increment = 0;
                    array[index].typeAnswer = 'radio';
                    value.answers.forEach((answerValue, answerIndex, answerArray) => {
                        if (answerValue.isRight) {
                            increment = increment + 1;
                        }
                    });
                    if (increment > 1) {
                        array[index].typeAnswer = 'check';
                    }
                });
                console.log(this.tests);
            
                $(document).ready(() => {
                    const rsc = (this.tests.length - 1);
    
                    if (this.isBuyProduct) {
                        $('#container-top').removeClass('container-premium');
                    } else {
                        $('#container-top').show()
                    }
                
                    $('.votes-slick').slick({
                        arrows: false, dots: true, infinite: false, swipe: false,
                        customPaging: function (slider, i) {
                            return '<img src="assets/image/dots/circle_o.png" height="16" width="16" />' +
                                '<img src="assets/image/dots/circle.png" height="16" width="16" />' +
                                '<img src="assets/image/dots/good.png" height="16" width="16" />' +
                                '<img src="assets/image/dots/bad.png" height="16" width="16" />' +
                                '<img src="assets/image/dots/dontknow.png" height="4" width="16" />';
                        },
                    });
                
                    $('.votes-item-list-check').click(function () {
                        const count_is_right = $('.slick-current .votes-item-list-check.is-right').length;
                    
                        if ($(this).find('i').hasClass('fa-square-o')) {
                            $(this).find('i').removeClass('fa-square-o');
                            $(this).find('i').addClass('fa-check-square-o');
                            if ($(this).hasClass('is-right')) {
                                $(this).addClass('is-check');
                            } else {
                                $(this).addClass('is-bad-check');
                            }
                        } else {
                            $(this).find('i').removeClass('fa-check-square-o');
                            $(this).find('i').addClass('fa-square-o');
                            if ($(this).hasClass('is-right')) {
                                $(this).removeClass('is-check');
                            } else {
                                $(this).removeClass('is-bad-check');
                            }
                        }
                    
                        if ($('.slick-current .votes-item-list .fa-check-square-o').length > 0) {
                            $('.slick-current .votes-item .votes-btn-container a').html('Ответить')
                                .removeClass('vote-dont-know')
                                .addClass('vote-answer');
                        } else {
                            $('.slick-current .votes-item .votes-btn-container a').html('Я не знаю :(')
                                .removeClass('vote-answer')
                                .addClass('vote-dont-know');
                            stat = 'dontknow';
                        }
                    
                        if ($('.slick-current .votes-item-list-check.is-check').length === count_is_right) {
                            stat = 'good';
                            goodAnswers++;
                        } else {
                            stat = 'bad';
                        }
                    
                        if ($('.slick-current .votes-item-list-check.is-bad-check').length > 0) {
                            stat = 'bad';
                        }
                    
                        console.log(count_is_right);
                    });
                
                    $('.votes-item-list-radio').click(function () {
                        $('.votes-item-list .votes-item-list-radio').find('i')
                            .removeClass('fa-check-circle-o')
                            .addClass('fa-circle-o');
                        $(this).find('i')
                            .removeClass('fa-circle-o')
                            .addClass('fa-check-circle-o');
                        $('.slick-current .votes-item .votes-btn-container a').html('Ответить')
                            .removeClass('vote-dont-know')
                            .addClass('vote-answer');
                    
                        stat = ($(this).hasClass('is-right')) ? 'good' : 'bad';
                        goodAnswers = ($(this).hasClass('is-right')) ? (goodAnswers + 1) : goodAnswers;
                    });
                
                    let num = 0;
                
                    $('.votes-item .votes-btn-container a').click(function () {
                        $('.slick-current .votes-status-accept-' + stat).css({'display': 'flex'});
                        $('.slick-current').find('.votes-title-line, .votes-item-subtitle, .votes-item-list').hide();
                        $('.slick-current .votes-item-title').html(answerText[stat]);
                        $('.slick-current .votes-item-answer').show();
                    
                        if (num < rsc) {
                            $(this).html('Продолжить').removeClass('vote-answer').addClass('vote-next');
                        } else {
                            $(this).html('Завершить тест').removeClass('vote-answer').addClass('vote-finish');
                        }
                        $('.slick-dots .slick-active').removeClass('slick-active').addClass('slick-' + stat);
                    
                        $('.slick-current .votes-item .votes-btn-container a.vote-next').click(function () {
                            $('.votes-slick').slick('slickGoTo', $(this).data('slide') + 1);
                            console.log($(this).data('slide') + '-' + stat);
                            setVotesProgress($(this).data('slide'), stat);
                            stat = 'dontknow';
                            num++;
                        });
                    
                        $('.slick-current .votes-item .votes-btn-container a.vote-finish').click(function () {
                            // Финал тестов
                            setVotesProgress($(this).data('slide'), stat);
                            console.log('Верных ответов: ' + goodAnswers);
                            component.setUrl()
                        });
                    });
                });
            }
        ).subscribe();

        function setVotesProgress(slideId, statusType) {
            voteProgress = localStorage.getItem(environment.voteProgress);
            if (!voteProgress || voteProgress === 'undefined') {
                voteProgress = [];
                progress = new VoteProgress(+_id, +slideId, statusType);
                voteProgress.push(progress);
                localStorage.setItem(environment.voteProgress, JSON.stringify(voteProgress));
                console.log(localStorage.getItem(environment.voteProgress));
            } else {
                voteProgress = JSON.parse(voteProgress);
                console.log(voteProgress);
                voteProgress.forEach((value, index) => {
                    if (value.voteId === +_id) {
                        progress = value;
                    }
                    if (value.slidetId === +slideId) {
                        progressType = value.statusType;
                    }
                });

                if (!progress) {
                    progress = new VoteProgress(+_id, +slideId, statusType);
                    voteProgress.push(progress);
                    localStorage.setItem(environment.voteProgress, JSON.stringify(voteProgress));
                } else {
                    if (statusType === 'good' || !voteProgress[slideId]) {
                        progress = new VoteProgress(+_id, +slideId, statusType);
                        voteProgress[slideId] = progress;
                        localStorage.setItem(environment.voteProgress, JSON.stringify(voteProgress));
                    }
                }
            }
        }
    }

    onBack() {
        this.setUrl();
    }
    
    setViewDescription(event: boolean) {
        // alert('привет')
        this.viewDescription = event;
    }
    
    ngOnDestroy(): void {
        this.subscribe.unsubscribe();
    }
    
    payment(product: string) {
        this.mainService.buyProduct(product)
            .then(() => {
                $('#container-top').removeClass('.container-premium');
            });
        
    }
    
    private setUrl(){
        let cards: Cards[] = JSON.parse(localStorage.getItem(environment.cards));
        cards.forEach(card => {
            if (card.id == +this.id) {
                let subjects: Subthemes[] = JSON.parse(localStorage.getItem(environment.subThemes));
                subjects.forEach(subject => {
                    if (card.subSubjectId == subject.id) {
                        console.log(subject)
                        // component.router.navigate(['/cards', ])
                        this.router.navigate(['/cards', subject.id, 'theme', subject.subjectId]);
                    }
                })
            }
        })
    }
    
    private printObject(o) {
        var out = '';
        for (var p in o) {
            out += p + ': ' + o[p] + '\n';
        }
        // alert(out);
    }


}
