import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {environment} from '../../environments/environment';
import {MainService} from '../shared/servises/main.service';
import {Cards} from "../shared/models/cards.model";
import {Router} from "@angular/router";

declare var $: any;

@Component({
    selector: 'app-favorite',
    templateUrl: './favorite.component.html',
    styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {

    @Input() favorite: [{card:Cards}];
    @Input() stepResult: [{ index: number, value: number }];
    currentCard: Cards;
    slickLightbox = [];

    constructor(private mainService: MainService, private router: Router) {
    }

    ngOnInit() {
        this.favorite = JSON.parse(localStorage.getItem(environment.favorite));
        console.log(this.favorite);

        $(document).ready(() => {
            $('.favorite-slick').slick({arrows: false, infinite: false});
            this.lsMoveToCard();
            $(document).on('click', '.text-yellow', (e: Event) => {
                let name = $(e.target).parent().siblings('.card-item-title').text();
                let slideId = (+$(e.target).closest('.card-item').attr('data-card'));
                localStorage.setItem(environment.currentSlide, slideId.toString());
                this.router.navigate(['/dictionary'],
                    {
                        queryParams: {search: $(e.target).text(), name: name}
                    });
            });
        });
    }

    closeLayout() {
        $('.shared').fadeOut(() => {
            $('.layout').fadeOut();
        });
    }

    removeFavorite() {
        const card: any = this.currentCard;
        console.log(card.id);
        const favorite: [{ card: Cards }] | string | any = JSON.parse(localStorage.getItem(environment.favorite));
        console.log(favorite);

        const favoriteArr = [];
        favorite.forEach((value, index, array) => {
            console.log(card.id);
            if (value.card.id !== card.id) {
                favoriteArr.push(value);
            }
        });
        console.log(favoriteArr);
        localStorage.setItem(environment.favorite, JSON.stringify(favoriteArr));

        $('.favorite-slick').slick('destroy');
        $('.card-item[data-card=' + card.id + ']').closest('app-card').remove();
        $('.favorite-slick').slick({arrows: false, infinite: false});
        $('.favorite-slick').find('app-card').css({'height':'100vh'})

        this.closeLayout();
    }
    
    setFavorite(card: Cards) {
		this.currentCard = card;
	}

    onBack() {
        this.router.navigate(['/menu']);
    }

    toggleCards() {
        let img = $('.container-top button img'),
            cardSlick = $('.cards-slick'),
            cardList = $('.cards-list');

        if (img.hasClass('hide-cards')) {
            img.removeClass('hide-cards').addClass('show-cards').attr('src', 'assets/image/show-plits.png')
            cardSlick.fadeOut(() => {
                cardList.fadeIn(() => {
                    cardList.css('display', 'flex')
                });
            });
        } else {
            img.removeClass('show-cards').addClass('hide-cards').attr('src', 'assets/image/hide-plits.png')
            cardList.fadeOut(() => {
                cardSlick.fadeIn();
            })
        }
    }
    
    setImageDescription(imageDescription: { id: number, imageDescription: string }) {
        
        if (this.slickLightbox.indexOf(imageDescription.id) == -1) {
            this.slickLightbox.push(imageDescription.id);
            $('#card-item-' + imageDescription.id).slickLightbox({
                itemSelector: '.card-item-img',
                navigateByKeyboard: false,
                caption: () => {
                    return $('<p/>', {
                        text: imageDescription.imageDescription
                    }).css({
                        'marginTop':'10px'
                    })[0].outerHTML;
                }
            });
            setTimeout(()=>{$('#card-item-' + imageDescription.id + ' .card-item-img').trigger('click')}, 200)
        }
    }
    
    //переход на карту, после возвращения с подробной инфы о карте
    lsMoveToCard() {
        let id = localStorage.getItem(environment.currentSlide);
        console.log(id);
        if (id !== 'null' && id !== 'undefined') {
            this.moveToCardId(+id)
        }
        
        localStorage.setItem(environment.currentSlide, null);
        
    }
    
    moveToCardId(cardId: number) {
        console.log(cardId);
        let id = $('.favorite-slick').find('.card-item[data-card=' + cardId + ']').parents('.slick-slide').attr('data-slick-index');
        console.log(id);
        $('.favorite-slick').slick('slickGoTo', +id);
    }

}
