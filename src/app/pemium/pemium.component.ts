import {Component, EventEmitter, Input, NgZone, OnInit, Output} from '@angular/core';
import {Subthemes} from "../shared/models/subthemes.model";
import {environment} from "../../environments/environment";
import {ActivatedRoute, Router} from "@angular/router";
import {Product} from "../shared/interfaces/product.inteface";

declare var inAppPurchase;


@Component({
    selector: 'app-pemium',
    templateUrl: './pemium.component.html',
    styleUrls: ['./pemium.component.css']
})
export class PemiumComponent implements OnInit {
    viewPremium: boolean = true;
    
    id: string;
    themeId: string;
    @Input() votesId: string;
    @Output() viewDescription = new EventEmitter<boolean>();
    
    constructor(private activatedRoute: ActivatedRoute,
                private router: Router,
                private ngZone: NgZone) {
    }
    
    ngOnInit() {
        this.id = this.activatedRoute.snapshot.params.id;
        this.themeId = this.activatedRoute.snapshot.params.subjectId;
        let component = this;
        inAppPurchase
            .restorePurchases()
            .then(function (data: Product[]) {
                component.viewPremium = (component.votesId != '205');
                // alert('проверка покупки');
                data.forEach((value) => {
                    if (value.productId == 'access3') {
                        // alert(123);
                        // alert('567888');
                        component.viewPremium = false;
                        component.viewDescription.emit(true);
                    }
                    // component.printObject(value)
                });
            })
            .catch(function (err) {
                // alert('ошибка проверки покупки')
            });

        inAppPurchase
            .getProducts(['access3'])
            .then(function (products: Product[]) {
                // alert('получение товара');
                // component.printObject(products[0]);
            })
            .catch(function (err) {
                // alert('ошибка');
                // component.printObject(err);
            });
    }
    
    closePremium() {
        console.log(this.router.url);
        this.viewPremium = false;
        this.viewDescription.emit(true);
    
        console.log(this.activatedRoute.snapshot.params);
        if (this.activatedRoute.snapshot.params.favorite == '1') {
            this.router.navigate(['/favorite']);
            return;
        }
        let subThemes: Subthemes[] = JSON.parse(localStorage.getItem(environment.subThemes));
        subThemes.forEach((value) => {
            if (value.id == +this.themeId) {
                this.router.navigate(['/cards', this.themeId, 'theme', value.subjectId]);
                return
            }
        });
    
        if (this.activatedRoute.snapshot.params.subjectId && this.activatedRoute.snapshot.params.subjectId.indexOf('favorite') != -1) {
            this.router.navigate(['/favorite']);
            return;
        }
    
        if (this.router.url.indexOf('votes') != -1) {
            this.ngZone.run(() => this.router.navigate([localStorage.getItem(environment.prevLinkDictionary)])).then();
            return;
        }
    
        if (this.router.url.indexOf('dictionary') != -1) {
            this.ngZone.run(() => this.router.navigate([localStorage.getItem(environment.prevLinkDictionary)])).then();
            return;
        }
    }
    
    payment(buy) {
        let component = this;
        inAppPurchase
            .buy(buy)
            .then(function (data) {
                component.viewPremium = false;
                component.viewDescription.emit(true);
            })
            .catch(function (err) {
                console.log(err);
            });
    }
    
    private printObject(o) {
        var out = '';
        for (var p in o) {
            out += p + ': ' + o[p] + '\n';
        }
        // alert(out);
    }
    
}
