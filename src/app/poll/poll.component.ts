import {Component, OnInit} from '@angular/core';
import {MainService} from '../shared/servises/main.service';
import {Polls} from '../shared/models/polls.model';
import {environment} from '../../environments/environment';
import {Router} from "@angular/router";

declare var $: any;

@Component({
    selector: 'app-poll',
    templateUrl: './poll.component.html',
    styleUrls: ['./poll.component.css']
})
export class PollComponent implements OnInit {

    polls: Polls[];
    isPoll: number = 0;

    constructor(private mainService: MainService,  private router: Router) {
    }

    ngOnInit() {
        const isPoll = localStorage.getItem(environment.pollAnsw);
        if (isPoll && isPoll !== 'undefined') {
            this.isPoll = +isPoll;
        }

        this.mainService.getPolls()
            .then((data: Polls[]) => {
                this.polls = data;
                console.log(this.polls);
            });
    }

    setPoll(elem: Element, id: number) {
        const self = this;
        const VoteButton = $('.vote-button');
        const poll = $(elem).closest('.poll-item');
        const pollItem = $('.poll-item');
        pollItem.removeClass('poll-active');
        poll.addClass('poll-active');
        VoteButton.show();

        VoteButton.click(function () {
            self.mainService.setPolls(id)
                .then((data: any) => {
                    if (data.voted) {
                        localStorage.setItem(environment.pollAnsw, String(id));
                        $(this).addClass('vote-complete');
                        pollItem.addClass('poll-complete');
                    }
                });
        });
    }
    
    onBack() {
        this.router.navigate(['/menu']);
    }

}
