import {Component, Input, OnInit} from '@angular/core';
import {ProgressBar} from "../../shared/models/progress-bar";
import {environment} from "../../../environments/environment";

declare var $: any;

@Component({
	selector: 'app-card-progress-bar',
	templateUrl: './card-progress-bar.component.html',
	styleUrls: ['./card-progress-bar.component.css']
})
export class CardProgressBarComponent implements OnInit {
	
	@Input() progress: ProgressBar;
	width: number;
	
	constructor() {
	}
	
	ngOnInit() {
		let component = this;
		this.width = (this.progress.countReadCards / this.progress.countCards) * 100;
		
		$(document).ready(() => {
			$('.cards-slick').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				
				if (component.progress.countReadCards < (nextSlide + 1)) {
					component.progress.countReadCards = nextSlide + 1;
					component.width = (component.progress.countReadCards / component.progress.countCards) * 100;
					let progressBar: ProgressBar[] = JSON.parse(localStorage.getItem(environment.progressBar));
					progressBar.forEach((value, index, array) => {
						if (value.subThemeId == component.progress.subThemeId) {
							console.log(value);
							array[index].countReadCards = component.progress.countReadCards;
						}
						
					});
					localStorage.setItem(environment.progressBar, JSON.stringify(progressBar))
				}
			});
		})
	}
	
}
