import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Cards} from "../../shared/models/cards.model";
import {MainService} from "../../shared/servises/main.service";
import {Votes} from "../../shared/models/votes";
import {environment} from "../../../environments/environment";

declare var $: any;

@Component({
	selector: 'app-card',
	templateUrl: './card.component.html',
	styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
	@Input() card: Cards;
	@Input() stepResult: [{ index: number, value: number }];
	@Input() isFavorite: boolean;
	@Output() favorite = new EventEmitter<Cards>();
	@Output() imageDescription = new EventEmitter<{ id: number, imageDescription: string }>();
	
	constructor(private mainService: MainService) {
	}
	
	ngOnInit() {
		if (this.isFavorite == undefined) this.isFavorite = false;
		
		this.card.text = this.card.mainText
			.replace(/</g, '[')
			.replace(/>/g, ']')
			.replace(/\[/g, '<span class="card text-yellow">')
			.replace(/\]/g, '</span>');
		if (this.card.cardType === 'Test') {
			this.mainService.getTests(this.card.id).then((data: Votes[]) => {
				let pre_length = 0;
				
				const voteProgress = JSON.parse(localStorage.getItem(environment.voteProgress));
				if (voteProgress && voteProgress !== 'undefined') {
					voteProgress.forEach((value, index) => {
						if (value.voteId === this.card.id && value.statusType === 'good') {
							pre_length++;
						}
					});
				}
				
				const length: number = data.length;
				this.card.testButtonName = (pre_length > 0) ? 'Подробнее' : 'Начать';
				this.card.name = 'Пройдено ' + pre_length + '/' + length;
				
			});
		}
	}
	
	addFavorite(card: Cards) {
		$('.layout').fadeIn(() => {
			$('.shared').fadeIn();
			console.log(123);
			this.favorite.emit(card);
		});
	}
	
	getWords(event: Event) {
		console.log(event)
	}
	
	onClick(event:Event) {
		if ($(event.target).closest('.favorite').length == 0) this.imageDescription.emit({id: this.card.id, imageDescription: this.card.imageDescription});
		return false;
	}
}
