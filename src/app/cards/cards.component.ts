import {Component, OnInit} from "@angular/core";
import {DataService} from "../shared/servises/data.service";
import {MainService} from "../shared/servises/main.service";
import {Cards} from "../shared/models/cards.model";
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from "../../environments/environment";
import {ProgressBar} from "../shared/models/progress-bar";

declare var $: any;

@Component({
	selector: 'app-cards',
	templateUrl: './cards.component.html',
	styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
	cards: Cards[];
	id: string;
	themeId: string;
	stepResultDefault: number = 0;
	stepResult: [{ index: number, value: number }];
	progressBar;
	progress: ProgressBar;
	loadPage: boolean = false;
	slickLightbox = [];
	
	textForFavoriteBtn: string = 'Добавить в избранное';
	
	favorite: Cards;
    
    isBuyProduct: boolean = false;
	
	constructor(
		private mainService: MainService,
		private dataService: DataService,
		private activatedRoute: ActivatedRoute,
		private router: Router) {
        this.mainService.getBuyProduct()
            .then((products) => {
                products.forEach((value) => {
                    if (value.productId == 'access3') {
                        // alert('продукт был ранее куплен')
                        this.isBuyProduct = true;
                    }
                    // this.printObject(value);
                });
            })
	}
	
	ngOnInit() {
		let component = this;
		this.id = this.activatedRoute.snapshot.params.id;
		this.themeId = this.activatedRoute.snapshot.params.themeId;
		this.cards = JSON.parse(localStorage.getItem(environment.cards));
		this.setProgressBar();
		this.setStepResult();
		localStorage.setItem(environment.prevLinkDictionary, this.router.url);
		$(document).ready(() => {
			$('.cards-slick').slick({arrows: false, infinite: false});
			$('.cards-slick').on('swipe', function (event, slick, direction) {
				let countShowingCards = parseInt(localStorage.getItem(environment.banner));
                if (countShowingCards >= 20 && !component.isBuyProduct) {
                    // alert('Вы попали на тв! Вы теперь звезда инета)');
                    // Start showing banners (atomatic when autoShowBanner is set to true)
                    // @ts-ignore
                    admob.createBannerView();
                    
                    // Request interstitial ad (will present automatically when autoShowInterstitial is set to true)
                    // @ts-ignore
                    admob.requestInterstitialAd();
                    
                    // Request rewarded ad (will present automatically when autoShowRewarded is set to true)
                    // @ts-ignore
                    admob.requestRewardedAd();
					countShowingCards = 0;
				}
				if (!countShowingCards) countShowingCards = 0;
				countShowingCards = countShowingCards + 1;
				localStorage.setItem(environment.banner, countShowingCards.toString());
			});
			
			component.lsMoveToCard();
			$(document).on('click', '.text-yellow', (e: Event) => {
				if (this.router.url.indexOf('theme') != -1) {
					let name = $(e.target).parent().siblings('.card-item-title').text();
					let slideId = (+$(e.target).closest('.card-item').attr('data-card'));
					localStorage.setItem(environment.currentSlide, slideId.toString());
					localStorage.setItem(environment.prevLinkDictionary, this.router.url);
					this.router.navigate(['/dictionary'],
						{
							queryParams: {search: $(e.target).text(), name: name}
						});
				}
			});
		});
		this.loadPage = true;
	}
	
	//переход на карту, после возвращения с подробной инфы о карте
	lsMoveToCard() {
		let id = localStorage.getItem(environment.currentSlide);
		if (id !== 'null' && id !== 'undefined') {
			this.moveToCardId(+id)
		}
		
		localStorage.setItem(environment.currentSlide, null);
		
	}
	
	moveToCardId(cardId: number) {
		let id = $('.cards-slick').find('.card-item[data-card=' + cardId + ']').parents('.slick-slide').attr('data-slick-index');
		$('.cards-slick').slick('slickGoTo', +id);
	}
	
	closeLayout() {
		$('.shared').fadeOut(() => {
			$('.layout').fadeOut();
		})
	}
	
	setFavorite(card: Cards) {
		this.favorite = card;
		let favorite: [{ card: Cards }] | string | any = JSON.parse(localStorage.getItem(environment.favorite));
		this.textForFavoriteBtn = 'Добавить в избранное';
		favorite.forEach((value, index, array) => {
			if (value.card.id == card.id) {
				this.textForFavoriteBtn = 'Удалить из избранного';
			}
		});
	}
	
	addFavorite() {
		let card = this.favorite,
			cardIdInArr;
		console.log(card);
		let favorite: [{ card: Cards }] | string | any = JSON.parse(localStorage.getItem(environment.favorite));
		if (!favorite || favorite == 'null' || favorite == 'undefined') {
			localStorage.setItem(environment.favorite, JSON.stringify([{card}]));
		} else {
			let flag: boolean = true;
			favorite.forEach((value, index, array) => {
				if (value.card.id == card.id) {
					flag = false;
					cardIdInArr = index;
				}
			});
			if (flag) {
				favorite.push({card});
				this.textForFavoriteBtn = 'Добавить в избранное';
			} else {
				favorite.splice(cardIdInArr, 1);
				this.textForFavoriteBtn = 'Удалить из избранного';
			}
			localStorage.setItem(environment.favorite, JSON.stringify(favorite));
		}
		
		this.closeLayout();
	}
	
	setImageDescription(imageDescription: { id: number, imageDescription: string }) {
		if (this.slickLightbox.indexOf(imageDescription.id) == -1) {
			this.slickLightbox.push(imageDescription.id);
			$('#card-item-' + imageDescription.id).slickLightbox({
				itemSelector: '.card-item-img',
				navigateByKeyboard: false,
				caption: () => {
					return $('<p/>', {
						text: imageDescription.imageDescription
					}).css({
						'marginTop': '10px',
						'font-family': 'Open Sans Regular'
					})[0].outerHTML;
				}
			});
			setTimeout(()=>{$('#card-item-' + imageDescription.id + ' .card-item-img').trigger('click')}, 200)
		}
	}
	
	//устанавливаем цифры для карточек-выводов (результаты)
	private setStepResult() {
		this.cards.forEach((value: Cards) => {
			if (value.cardType == 'OtherResults') {
				this.stepResultDefault = this.stepResultDefault + 1;
				if (!this.stepResult) {
					this.stepResult = [{index: value.id, value: this.stepResultDefault}];
				} else {
					this.stepResult.push({index: value.id, value: this.stepResultDefault})
				}
				
			}
		});
	}
	
	private setProgressBar() {
		this.progressBar = localStorage.getItem(environment.progressBar);
		if (!this.progressBar || this.progressBar == 'undefined') {
			this.progressBar = [];
			this.progress = new ProgressBar(+this.id, +this.themeId, this.cards.length, 1)
			this.progressBar.push(this.progress);
			localStorage.setItem(environment.progressBar, JSON.stringify(this.progressBar));
			console.log(localStorage.getItem(environment.progressBar))
		} else {
			this.progressBar = JSON.parse(this.progressBar);
			this.progressBar.forEach((value, index) => {
				if (value.subThemeId == +this.id) {
					this.progress = value;
				}
			});
			
			if (!this.progress) {
				this.progress = new ProgressBar(+this.id, +this.themeId, this.cards.length, 1)
				this.progressBar.push(this.progress);
				localStorage.setItem(environment.progressBar, JSON.stringify(this.progressBar));
			}
		}
	}
}
