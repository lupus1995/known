import {Component, Input, OnInit} from '@angular/core';
import {Subthemes} from "../../shared/models/subthemes.model";
import {environment} from "../../../environments/environment";
import {ActivatedRoute, Router} from "@angular/router";

declare var $: any;

@Component({
	selector: 'app-cards-top',
	templateUrl: './cards-top.component.html',
	styleUrls: ['./cards-top.component.css']
})
export class CardsTopComponent implements OnInit {
	
	@Input() id: string;
	subThemes: Subthemes[];
	name: string;
	
	constructor(private activatedRoute: ActivatedRoute, private router: Router) {
	}
	
	ngOnInit() {
		this.subThemes = JSON.parse(localStorage.getItem(environment.subThemes));
		
		if (this.id) {
			this.subThemes.forEach((value, index) => {
				if (value.id == +this.id) {
					this.name = value.name
				}
			});
		} else {
			this.name = 'Избранное'
		}
	}
	
	onBack() {
		localStorage.setItem(environment.currentThemes, this.activatedRoute.snapshot.params.themeId);
		console.log(this.router.url);
		let url: string;
		if (this.router.url == '/favorite') url = '/menu';
		if (this.router.url.indexOf('cards') != -1) url = '/main';
		this.router.navigate([url]);
	}
	
	toggleCards() {
		let img = $('.container-top button img'),
			cardSlick = ($('.cards-slick').length > 0) ? $('.cards-slick') : $('.favorite-slick'),
			cardList = $('.cards-list');
		
		if (img.hasClass('hide-cards')) {
			img.removeClass('hide-cards').addClass('show-cards').attr('src', 'assets/image/show-plits.png')
			cardSlick.fadeOut(() => {
				cardList.fadeIn(() => {
					cardList.css('display', 'flex')
				});
			});
		} else {
			img.removeClass('show-cards').addClass('hide-cards').attr('src', 'assets/image/hide-plits.png')
			cardList.fadeOut(()=>{
				cardSlick.fadeIn();
			})
		}
	}
	
}
