import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Cards} from "../../shared/models/cards.model";

declare var $:any;

@Component({
	selector: 'app-card-list',
	templateUrl: './card-list.component.html',
	styleUrls: ['./card-list.component.css']
})
export class CardListComponent implements OnInit {
	
	@Input() card: Cards;
	@Output() cardId = new EventEmitter<number>();
	
	constructor() {
	}
	
	ngOnInit() {
	}
	
	onClick(cardId: number) {
		let img = $('.container-top button img'),
			cardSlick = ($('.cards-slick').length > 0) ? $('.cards-slick') : $('.favorite-slick'),
			cardList = $('.cards-list');
		img.removeClass('show-cards').addClass('hide-cards').attr('src', 'assets/image/hide-plits.png')
		cardList.fadeOut(()=>{
			cardSlick.fadeIn();
		});
		this.cardId.emit(cardId);
	}
	
}
