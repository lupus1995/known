import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-private',
    templateUrl: './private.component.html',
    styleUrls: ['./private.component.css']
})
export class PrivateComponent implements OnInit {
    
    constructor(private router: Router) {
    }
    
    ngOnInit() {
        console.log(123);
    }
    
    onBack() {
        this.router.navigate(['/menu']);
    }
    
}
