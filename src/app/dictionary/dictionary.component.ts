import {Component, OnInit} from '@angular/core';
import {MainService} from "../shared/servises/main.service";
import {ActivatedRoute, Router} from "@angular/router";

declare var $:any;

@Component({
	selector: 'app-dictionary',
	templateUrl: './dictionary.component.html',
	styleUrls: ['./dictionary.component.css']
})
export class DictionaryComponent implements OnInit {
	
	text:string;
	name:string;
	
	constructor(
		private mainService: MainService,
		private activatedRoute: ActivatedRoute,
		private router: Router
	) {
	}
	
	ngOnInit() {
		let query: string = this.activatedRoute.snapshot.queryParams.search;
		this.name = this.activatedRoute.snapshot.queryParams.name;
		this.mainService
			.getDictionary(query)
			.then((response: string) => {
				this.text = response;
			});
	}
	
}
