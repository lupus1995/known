import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {HttpClientModule} from "@angular/common/http"

import {ThemesComponent} from "./main/themes/themes.component";
import {MainComponent} from "./main/main.component";
import {MainRoutingModule} from "./main/main-routing.module";
import {SubthemesComponent} from './main/subthemes/subthemes.component';
import {TopComponent} from './main/top/top.component';
import {MenuComponent} from './menu/menu.component';
import {CardsComponent} from './cards/cards.component';
import {CardComponent} from './cards/card/card.component';
import {CardDescriptionComponent} from './card-description/card-description.component';
import {CardDescriptionTextComponent} from './card-description/card-description-text/card-description-text.component';
import {CardDescriptionHeaderComponent} from './card-description/card-description-header/card-description-header.component';
import {CardDescriptionTextInBoxComponent} from './card-description/card-description-text-in-box/card-description-text-in-box.component';
import {CardDescriptionImageComponent} from './card-description/card-description-image/card-description-image.component';
import {CardsTopComponent} from './cards/cards-top/cards-top.component';
import {CardDescriptionTopComponent} from './card-description/card-description-top/card-description-top.component';
import {FormsModule} from "@angular/forms";
import {CardProgressBarComponent} from './cards/card-progress-bar/card-progress-bar.component';
import {CardListComponent} from './cards/card-list/card-list.component';
import {VotesComponent} from './votes/votes.component';
import {HelpComponent} from './menu/help/help.component';
import {CommandComponent} from './menu/command/command.component';
import {FavoriteComponent} from './favorite/favorite.component';
import {PollComponent} from './poll/poll.component';
import {DictionaryComponent} from './dictionary/dictionary.component';
import {MaterialModule} from "./material.module";
import {SubthemesImageComponent} from "./main/subthemes/subthemes-image/subthemes-image.component";
import {PemiumComponent} from "./pemium/pemium.component";
import {SupportComponent} from "./menu/support/support.component";
import {PrivateComponent} from "./private/private.component";

@NgModule({
	declarations: [
		AppComponent,
		ThemesComponent,
		MainComponent,
		SubthemesComponent,
		TopComponent,
		MenuComponent,
		CardsComponent,
		CardComponent,
		CardDescriptionComponent,
		CardDescriptionTextComponent,
		CardDescriptionHeaderComponent,
		CardDescriptionTextInBoxComponent,
		CardDescriptionImageComponent,
		CardsTopComponent,
		CardDescriptionTopComponent,
		CardProgressBarComponent,
		CardListComponent,
		CardProgressBarComponent,
		VotesComponent,
		HelpComponent,
		CommandComponent,
		FavoriteComponent,
		PollComponent,
		DictionaryComponent,
        SubthemesImageComponent,
        PemiumComponent,
        SupportComponent,
        PrivateComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		MainRoutingModule,
		HttpClientModule,
		FormsModule,
		MaterialModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
